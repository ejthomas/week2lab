package cs.mad.flashcards.entities

data class FlashcardSet(val item: String) {
    val blankflashcardset = ""
    val flashsetlist = listOf(blankflashcardset,blankflashcardset,blankflashcardset,blankflashcardset,blankflashcardset,blankflashcardset,blankflashcardset,blankflashcardset,blankflashcardset,blankflashcardset)

    fun returnFlashcardSet(): List<String> {
        return flashsetlist

    }
}