package cs.mad.flashcards.entities

data class Flashcard(val item: String) {
    val blankflashcard = ""
    val flashlist = listOf(blankflashcard,blankflashcard,blankflashcard,blankflashcard,blankflashcard,blankflashcard,blankflashcard,blankflashcard,blankflashcard,blankflashcard)

    fun returnFlashcard(): List<String> {
        return flashlist

    }
}